#pragma once

#include<string>
#include<iostream>
#include "Queue.h"
using namespace std;

const int MAX_SIZE = 101;
typedef string KeyType;


class Dictionary
{
private:
	struct Node
	{
		Queue item;	// data item
	};

	Node* items[MAX_SIZE];
	int  size;			// number of items in the Dictionary

public:

	// constructor
	Dictionary();

	// destructor
	~Dictionary();

	int hash(KeyType key);

	// add a new item with the specified key to the Dictionary
	bool add(KeyType newKey, Booking newItem);

	// remove an item with the specified key in the Dictionary
	void remove(KeyType key);

	bool set(int index, Queue newQueue);

	// get an item with the specified key in the Dictionary (retrieve)
	// return the item with the specified key from the Dictionary
	Queue get(KeyType key);

	void getAllBookingsByRoom(string s);

	void getAllBookingsByMonth(string mth);

	void getAllBookingsByDate(string date);

	// return true if the Dictionary is empty; otherwise returns false
	bool isEmpty();

	// return the number of items in the Dictionary
	int getLength();

	// display the items in the Dictionary
	void print();
};
