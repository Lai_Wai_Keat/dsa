// DSA_P02_Team4_BookingSystem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>
#include "Booking.h"
#include "Queue.h"
#include "Dictionary.h"
#include "RoomDictionary.h"
#include "List.h"
#include <chrono>
#include <ctime>
#include <iomanip>

using namespace std;
void menu();

int main()
{
    Queue bookingQueue;
    Dictionary bookingInfo;

    ifstream BookingFile;
    BookingFile.open("bookings.csv");

    string id;
    int bookingID;
    int guestNum;
    string bookingDate;
    string name;
    string roomNo;
    string roomType;
    string status;
    string checkIn;
    string checkOut;
    string guestNo;
    string requests;
    string line;
    string columndata;

    int count = 0;
    while (!BookingFile.eof())
    {
        getline(BookingFile, id, ',');
        getline(BookingFile, bookingDate, ',');
        getline(BookingFile, name, ',');
        getline(BookingFile, roomNo, ',');
        getline(BookingFile, roomType, ',');
        getline(BookingFile, status, ',');
        getline(BookingFile, checkIn, ',');
        getline(BookingFile, checkOut, ',');
        getline(BookingFile, guestNo, ',');
        getline(BookingFile, requests, '\n');

        if (count == 1)
        {
            stringstream bid(id);
            int bookingID = 0;
            bid >> bookingID;
            stringstream gno(guestNo);
            int guestNum = 0;
            gno >> guestNum;
            ItemType b(bookingID, bookingDate, name, roomNo, roomType, status, checkIn, checkOut, guestNum, requests);

            string room = b.getRoomNo();
            if (room == "" || room == " ")
            {
                bookingQueue.enqueue(b);
            }
            else
            {
                bookingInfo.add(room, b);
            }
        }
        else
        {
            count++;
        }
    }
    BookingFile.close();

    ifstream RoomFile;
    RoomFile.open("Rooms.csv");

    string rm;
    string rt;
    string cost;

    RoomDictionary roomInfo;

    int num = 0;
    while (!RoomFile.eof())
    {
        getline(RoomFile, rm, ',');
        getline(RoomFile, rt, ',');
        getline(RoomFile, cost, '\n');

        if (num == 1)
        {
            stringstream rcost(cost);
            int roomCost = 0;
            rcost >> roomCost;
            roomInfo.add(rt, rm);
        }
        else
        {
            num++;
        }
    }

    while (true) {
        menu();
        int option;
        cin >> option;

        if (option == 1)
        {
            int id;
            cout << "Enter Booking ID : ";
            cin >> id;
            Booking b;
            Booking newBook;
            List roomList;
            Queue roomQueue;
            Queue resultQueue;
            if (bookingQueue.isIDExist(id))
            {
                
                bookingQueue.getByID(id, b);
                roomList = roomInfo.get(b.getRoomType());
                int added = 0;
                for (int t = 0; t < roomList.getLength(); t++)
                {
                    int number;
                    roomQueue = bookingInfo.get(roomList.get(t));
                    resultQueue = roomQueue.CheckDateOfBookings(b, added);
                    number = bookingInfo.hash(roomList.get(t));
                    bookingInfo.set(number, resultQueue);
                    if (added == 1)
                    {
                        resultQueue.getLast(newBook);
                        cout << newBook.getGuestName() << " has checked into " << newBook.getRoomNo() << endl;
                        bookingQueue.dequeueSpecific(b);
                        break;
                    }
                }
                if (added == 0)
                {
                    cout << "Check In Unsuccessful" << endl;
                }
                
            }
            else
            {
                cout << "No Matching records\n" << endl;
            }

        }
        else if (option == 2)
        {
            Booking b;
            bookingQueue.getLast(b);
            int newid = b.getid() + 1;
            string n;
            string rmType;
            string cinDate;
            string coutDate;
            int gnumber;
            string req = " ";

            cout << "Enter You Name : ";
            cin >> n;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Enter your desired Room Type : ";
            getline(cin, rmType);
            cout << "When would you like to Check In (dd/mm/yyyy): ";
            cin >> cinDate;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "When would you like to Check-Out (dd/mm/yyyy): ";
            cin >> coutDate;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Number of guests staying : ";
            cin >> gnumber;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Any Special Requests : ";
            cin >> req;
            cin.ignore(numeric_limits<streamsize>::max(), '\n');

            struct tm newtime;
            time_t now = time(0);
            localtime_s(&newtime, &now);
            char date[100];
            strftime(date, 100, "%d/%m/%Y %H:%M:%S", &newtime);

            // file pointer
            fstream fout;
            // opens an existing csv file or creates a new file.
            fout.open("Bookings.csv", ios::out | ios::app);
            // Insert the data to file
            fout << "\n" << newid << "," << date << "," << n << "," << "" << ","  << rmType << ","<< "Booked" << "," << cinDate << "," << coutDate << "," << gnumber << "," << req;
            ItemType newBooking(newid, date, n, roomNo, rmType, "Booked", cinDate, coutDate, gnumber, req);
            bookingQueue.enqueue(newBooking);
        }
        else if (option == 3)
        {
            string date;
            cout << "Enter Date (dd/mm/yyyy): ";
            cin >> date;
            bookingInfo.getAllBookingsByDate(date);
        }
        else if (option == 4)
        {
            int mth, yr;
            string mthArray[12] = { "January","February","March","April","May","June","July","August","September","October","November","December" };
            string yrArray[3] = { "2020","2021","2022" };

            for (int i = 0; i < sizeof(mthArray) / sizeof(mthArray[0]); i++)
            {
                cout << "[" << i + 1 << "]" << mthArray[i] << endl;
            }
            cout << "Choose Month: ";
            cin >> mth;

            for (int x = 0; x < sizeof(yrArray) / sizeof(yrArray[0]); x++)
            {
                cout << "[" << x + 1 << "] " << yrArray[x] << endl;
            }
            cout << "Enter Year: ";
            cin >> yr;

            string month;

            if (mth < 10)
            {
                month = "/" + to_string(mth);
            }
            else
            {
                month = to_string(mth);
            }

            string mthyr = month + "/" + yrArray[yr - 1];

            bookingInfo.getAllBookingsByMonth(mthyr);

        }
        else if (option == 5)
        {
            string roomNum;
            cout << "Enter Room Number (3 digits): ";
            cin >> roomNum;
            roomNum = "Room " + roomNum;
            bookingInfo.getAllBookingsByRoom(roomNum);
            cout << "\n";
        }
        else if (option == 6)
        {
            string typeArr[4] = { "Deluxe City View", "Executive Sea View", "President Suite", "Standard City View" };
            int countArr[4] = {};

            List displayList;
            Queue countQueue;
            for (int t = 0; t < sizeof(typeArr) / sizeof(typeArr[0]); t++)
            {
                int count = 0;
                displayList = roomInfo.get(typeArr[t]);
                for (int d = 0; d < displayList.getLength(); d++)
                {
                    countQueue = bookingInfo.get(displayList.get(d));
                    count += countQueue.getSize();
                }
                countArr[t] = count;
            }
            int index = 0;
            for (int i = 0; i < 4; i++)
            {
                if (countArr[0] < countArr[i])
                {
                    countArr[0] = countArr[i];
                    index = i;
                }
            }
            cout << "The Most Popular Room Type is " << typeArr[index] << " with " << countArr[0] << " bookings" << endl;
        }
        else if (option == 0)
        {
            break;
        }
        else
        {
            cout << "Invalid Option" << endl;
        }
    }
}

void menu()
{
    cout << "----------------Menu----------------" << endl;
    cout << "[1] Check-In Guest" << endl;
    cout << "[2] Add New Booking" << endl;
    cout << "[3] Display Guests by Date" << endl;
    cout << "[4] Display Dates of Rooms occupied by Month" << endl;
    cout << "[5] Display Guest Records by Room Number" << endl;
    cout << "[6] Display Most Popular Room Type" << endl;
    cout << "[0] Exit" << endl;
    cout << "------------------------------------" << endl;
    cout << "Enter Your Option : ";
}