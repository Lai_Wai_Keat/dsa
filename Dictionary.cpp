#include "Dictionary.h"
#include <sstream>
#include <string>
#include "Booking.h"
#include <time.h>
#include <iomanip>


// constructor
Dictionary::Dictionary()
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		items[i] = NULL;
	}
	size = 0;
}

// destructor is to remove dynamic memory
// static memory is destroyed when program ends, but not dynamic memory
// programmer does not call destructor. It is auto called by program when it ends
Dictionary::~Dictionary()
{
	// to be coded
}

int Dictionary::hash(KeyType key)
{
	string s = key.replace(0, 5, "");
	int intValue = atoi(s.c_str());

	int value = intValue % 101;
	return value;
}

bool Dictionary::add(KeyType newKey, Booking newItem)
{
	int index = hash(newKey);

	Queue tempQueue;

	if (items[index] == NULL)
	{
		tempQueue.enqueue(newItem);
		Node* newNode = new Node;
		newNode->item = tempQueue;
		items[index] = newNode;
	}
	else
	{
		Node* current = items[index];
		Queue replaceQueue;

		tm newResult;
		char* ntString;
		string ns = newItem.getCheckInDate();
		ntString = &ns[0];
		sscanf_s(ntString, "%d/%d/%4d",
			&newResult.tm_mday, &newResult.tm_mon, &newResult.tm_year);

		tm checkInDate2 = newResult;
		int date2 = checkInDate2.tm_year * 10000 + checkInDate2.tm_mon * 100 + checkInDate2.tm_mday;

		while (!current->item.isEmpty())
		{
			Booking b;
			current->item.getFront(b);
			Booking last;
			current->item.getLast(last);

			tm result;
			char* tString;
			string s = b.getCheckInDate();
			tString = &s[0];
			sscanf_s(tString, "%d/%d/%4d",
				&result.tm_mday, &result.tm_mon, &result.tm_year);

			tm checkInDate1 = result;
			int date1 = checkInDate1.tm_year * 10000 + checkInDate1.tm_mon * 100 + checkInDate1.tm_mday;

			if (date2 > date1)
			{
				replaceQueue.enqueue(b);
				current->item.dequeue();

				if (last.getid() == b.getid())
				{
					replaceQueue.enqueue(newItem);
				}
			}
			else
			{
				replaceQueue.enqueue(newItem);
				while (!current->item.isEmpty())
				{
					current->item.getFront(b);
					replaceQueue.enqueue(b);
					current->item.dequeue();
				}
			}
		}
		Node* newNode = new Node;
		newNode->item = replaceQueue;
		items[index] = newNode;
		delete current;
	}
	size++;
	return true;
}



// remove an item with the specified key in the Dictionary
void Dictionary::remove(KeyType key)
{
	// to be coded
}

bool Dictionary::set(int index, Queue newQueue)
{
	Node* newNode = new Node;
	newNode->item = newQueue;
	items[index] = newNode;
	return true;
}

// get an item with the specified key in the Dictionary (retrieve)

Queue Dictionary::get(KeyType key)
{
	int index = hash(key);

	Queue emptyQueue;

	if (items[index] == NULL)
	{
		cout << "No records" << endl;
		return emptyQueue;
	}
	else
	{
		Node* current = items[index];
		return current->item;
	}
}

void Dictionary::getAllBookingsByRoom(string s)
{
	int index = hash(s);

	if (items[index] != NULL)
	{
		Node* current = items[index];
		Queue setQueue;
		current->item.displayItems();
	}
}

void Dictionary::getAllBookingsByMonth(string mth)
{
	cout << "\n";
	for (int i = 0; i < 20; i++)
	{
		Booking b;
		if (items[i] != NULL)
		{
			Node* current = items[i];
			Queue setQueue;
			setQueue = current->item.displayByMonth(mth);
			set(i, setQueue);
		}
	}
	cout << "\n";
}

void Dictionary::getAllBookingsByDate(string date)
{
	cout << "\n";
	for (int i = 0; i < 20; i++)
	{
		Booking b;
		if (items[i] != NULL)
		{
			Node* current = items[i];
			Queue setQueue;
			setQueue = current->item.displayByDate(date);
			set(i, setQueue);
		}
	}
	cout << "\n";
}

// check if the Dictionary is empty
bool Dictionary::isEmpty()
{
	return size == 0;
}

// check the size of the Dictionary
int Dictionary::getLength()
{
	return size;
}

// display the items in the Dictionary
void Dictionary::print()
{
	// to be coded
}