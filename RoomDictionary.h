#pragma once

#include<string>
#include<iostream>
#include "Queue.h"
#include "List.h"
using namespace std;

const int ROOM_MAX_SIZE = 101;
typedef string KeyType;

class RoomDictionary
{
private:
	struct Node
	{
		KeyType	 key;   // search key
		string item;	// data item
		Node* next;	// pointer pointing to next item with same search key
	};

	Node* items[ROOM_MAX_SIZE];
	int  size;			// number of items in the Dictionary

public:

	// constructor
	RoomDictionary();

	// destructor
	~RoomDictionary();

	int hash(KeyType key);

	// add a new item with the specified key to the Dictionary
	bool add(KeyType newKey, string newItem);

	// remove an item with the specified key in the Dictionary
	void remove(KeyType key);

	// get an item with the specified key in the Dictionary (retrieve)
	// return the item with the specified key from the Dictionary
	List get(KeyType key);

	// return true if the Dictionary is empty; otherwise returns false
	bool isEmpty();

	// return the number of items in the Dictionary
	int getLength();

	// display the items in the Dictionary
	void print();
};
