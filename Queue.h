// Queue.h - Specification of Queue ADT (Pointer-based)
#pragma once

#include<string>
#include<iostream>
#include "Booking.h" 
using namespace std;

typedef Booking ItemType;

class Queue
{
private:
    struct Node
    {
      ItemType obj;	// item
      Node  *next;	// pointer pointing to next item
    };
	int size;
	Node *frontNode;	// point to the first item
	Node *backNode;	// point to the last item


public:
	// constructor
	Queue();

    ~Queue();

	// enqueue (add) item at the back of queue
	bool enqueue(ItemType item);

	// dequeue (remove) item from front of queue
	bool dequeue();

    // dequeue (remove) and retrieve item from front of queue
	bool dequeue(ItemType &item);

	bool dequeueSpecific(ItemType& item);

	// retrieve (get) item from front of queue
	void getFront(ItemType& item);

	// retrieve (get) item from Last of queue
	void getLast(ItemType &item);

	void getByID(int id, ItemType &item);

	int getSize();

	// check if the queue is empty
	bool isEmpty(); 

	bool isIDExist(int id);

    // display items in queue from front to back
    void displayItems();

	Queue displayByDate(string date);

	Queue displayByMonth(string mthyr);

	Queue CheckDateOfBookings(ItemType item, int& value);
}; 
