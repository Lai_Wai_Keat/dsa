// Booking.cpp - Implementation of Booking class
#include "Booking.h"

Booking::Booking() {}

Booking::Booking(int id, string date, string n, string rno, string rt, string s, string ci, string co, int gno, string req)
{
	bookingID = id;
	bookingDate = date;
	guestName = n;
	roomNo = rno;
	roomType = rt;
	status = s;
	checkIn = ci;
	checkOut = co;
	guestNo = gno;
	requests = req;
}

string Booking::getGuestName() { return guestName; }

string Booking::getRequests() { return requests; }

int Booking::getid() { return bookingID; }

string Booking::getBookingDate() { return bookingDate; }

string Booking::getRoomNo() { return roomNo; }

string Booking::getRoomType() { return roomType; }

string Booking::getCheckInDate() { return checkIn; }

string Booking::getCheckOutDate() { return checkOut; }

string Booking::getStatus() { return status; }

void Booking::setStatus(string s) { status = s; }

void Booking::setRoomNo(string rno) { roomNo = rno; }