#pragma once

#include<string>
#include<iostream>
using namespace std;

class Booking
{
private:
	int bookingID;
	string bookingDate; // to be confirmed to be either string or datetime
	string guestName;
	string roomNo;
	string roomType;
	string status;
	string checkIn; // to be confirmed to be either string or datetime
	string checkOut; // to be confirmed to be either string or datetime
	int guestNo;
	string requests;

public:

	Booking();

	Booking(int id, string date, string n, string rno, string rt, string s, string ci, string co, int gno, string req);

	string getGuestName();

	string getRequests();

	int getid();

	string getBookingDate();

	string getRoomNo();

	string getRoomType();

	string getCheckInDate();

	string getCheckOutDate();

	string getStatus();

	void setStatus(string s);

	void setRoomNo(string rno);

	// further functions to be implemented

};
