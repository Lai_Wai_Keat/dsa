#include "RoomDictionary.h"
#include "List.h"

// constructor
RoomDictionary::RoomDictionary()
{
	for (int i = 0; i < ROOM_MAX_SIZE; i++)
	{
		items[i] = NULL;
	}
	size = 0;
}

// destructor
RoomDictionary::~RoomDictionary(){}

int charvalue(char c) // eg. 'A'
{
	if (isalpha(c))
	{
		if (isupper(c))
			return (int)c - (int)'A';		// ascii code of c minus
		else								// ascii code of 'A' (65)
			return (int)c - (int)'a' + 26;	// ascii code of 'a' (97)
	}
	else
		return -1;
}

int RoomDictionary::hash(KeyType key)
{
	int total = charvalue(key[0]);
	for (int i = 1; i < key.size(); i++)
	{
		total = total * 52 + charvalue(key[i]);
		total %= ROOM_MAX_SIZE;
	}
	return total;
}

// add a new item with the specified key to the Dictionary
bool RoomDictionary::add(KeyType newKey, string newItem)
{
	int index = hash(newKey);

	if (items[index] == NULL)
	{
		Node* newNode = new Node;
		newNode->item = newKey;
		newNode->key = newItem;
		newNode->next = NULL;

		items[index] = newNode;
	}
	else
	{
		Node* current = items[index];

		if (current->key == newItem)
		{
			return false;
		}
		while (current->next != NULL)
		{
			current = current->next;
			if (current->key == newItem)
			{
				return false;
			}
		}
		Node* newNode = new Node;
		newNode->item = newKey;
		newNode->key = newItem;
		newNode->next = NULL;

		current->next = newNode;
	}
	size++;
	return true;
}

// remove an item with the specified key in the Dictionary
void RoomDictionary::remove(KeyType key)
{
	int index = hash(key);

	if (items[index] != NULL)
	{
		Node* current = items[index];
		Node* previous = items[index];
		if (current->key == key)
		{
			items[index] = current->next;
			delete current;
			size--;
		}
		else
		{
			while (current->next != NULL)
			{
				previous = current;
				current = current->next;
				if (current->key == key)
				{
					previous->next = current->next;
					current->next = NULL;
					delete current;
					size--;
				}

			}
		}
	}
}

// get an item with the specified key in the Dictionary (retrieve)
// return the item with the specified key from the Dictionary
List RoomDictionary::get(KeyType key)
{
	List roomList;
	int index = hash(key);

	if (items[index] == NULL)
	{
		cout << "Item does not exist" << endl;
		return roomList;
	}
	else
	{
		Node* current = items[index];

		while (current->next != NULL)
		{
			if (current->item == key)
			{
				roomList.add(current->key);
			}
			current = current->next;
		}
		if (current->next == NULL)
		{
			if (current->item == key)
			{
				roomList.add(current->key);
			}
		}
		
	}
	return roomList;
}

// return true if the Dictionary is empty; otherwise returns false
bool RoomDictionary::isEmpty() { return size == 0; }

// return the number of items in the Dictionary
int RoomDictionary::getLength() { return size; }

// display the items in the Dictionary
void RoomDictionary::print()
{
	if (!isEmpty())
	{
		for (int i = 0; i < ROOM_MAX_SIZE; i++)
		{
			if (items[i] != NULL)
			{
				Node* temp = items[i];
				cout << temp->item << endl;
				while (temp != NULL)
				{
					temp = temp->next;
				}
			}
		}
	}
}