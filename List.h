#pragma once
#include <iostream>
using namespace std;

const int LIST_MAX_SIZE = 100;

class List
{
private:
    string items[LIST_MAX_SIZE];
    int size;

public:
    // constructor
    List();

    // add an item to the back of the list (append)
    // pre : size < MAX_SIZE
    // post: item is added to the back of the list
    //       size of list is increased by 1
    bool add(string item);

    // add an item at a specified position in the list (insert)
     // pre : 0 <= index < size && size < MAX_SIZE
     // post: item is added to the specified position in the list
     //           size of list is increased by 1
    bool add(int index, string item);

    // remove an item at a specified position in the list
    // pre : 0 <= index < size
    // post: item is removed at the specified position in the list
    //           size of list is decreased by 1
    void remove(int index);

    // get an item at a specified position of the list (retrieve)
    // pre : 0 <= index < size
    // post: none
    string get(int index);

    int getLength();

};