#include "Queue.h"
#include <iomanip>

Queue::Queue() { frontNode = NULL, backNode = NULL, size = 0; }

Queue::~Queue() {};

// enqueue (add) obj at the back of queue
bool Queue::enqueue(Booking obj)
{
	Node* newNode = new Node;
	newNode->obj = obj;
	newNode->next = NULL;

	if (isEmpty())
	{
		frontNode = newNode;
	}
	else
	{
		backNode->next = newNode;
	}
	backNode = newNode;
	size++;
	return true;
};

// dequeue (remove) obj from front of queue
bool Queue::dequeue()
{
	if (frontNode == backNode)
	{
		frontNode = NULL;
		backNode = NULL;
		size--;
		return true;
	}
	else
	{
		Node* temp = frontNode;
		frontNode = frontNode->next;
		temp->next = NULL;
		delete temp;
		size--;
		return true;
	}
};

// dequeue (remove) and retrieve obj from front of queue
bool Queue::dequeue(ItemType& obj)
{
	if (!isEmpty())
	{
		if (frontNode->next == backNode->next)
		{
			obj = frontNode->obj;
			frontNode = NULL;
			backNode = NULL;
			size--;
			return true;
		}
		else
		{
			Node* temp = frontNode;
			frontNode = frontNode->next;
			temp->next = NULL;
			delete temp;
			size--;
			return true;
		}
	}
	else
	{
		cout << "The Queue is empty.\n";
		return false;
	}
};

bool Queue::dequeueSpecific(ItemType& obj)
{
	Queue tempQueue;
	Booking b;
	if (!isEmpty())
	{
		while (!isEmpty())
		{
			getFront(b);
			if (b.getid() != obj.getid())
			{
				tempQueue.enqueue(b);
			}
			dequeue();
		}
		while (!tempQueue.isEmpty())
		{
			tempQueue.getFront(b);
			enqueue(b);
			tempQueue.dequeue();
		}
		size--;
		return true;
	}
	else
	{
		return false;
	}
}

// retrieve (get) obj from last of queue
void Queue::getLast(ItemType& obj)
{
	bool sucess = !isEmpty();
	if (sucess)
	{
		obj = backNode->obj;
	}
};

// retrieve (get) obj from front of queue
void Queue::getFront(ItemType& obj)
{
	bool sucess = !isEmpty();
	if (sucess)
	{
		obj = frontNode->obj;
	}
};

void Queue::getByID(int id, ItemType& item)
{
	Queue tempQueue;
	Booking b;
	if (!isEmpty())
	{
		while (!isEmpty())
		{
			getFront(b);
			if (b.getid() == id)
			{
				item = b;
			}
			tempQueue.enqueue(b);
			dequeue();
		}
		while (!tempQueue.isEmpty())
		{
			tempQueue.getFront(b);
			enqueue(b);
			tempQueue.dequeue();
		}
	}
}
int Queue::getSize()
{
	return size;
}

// check if the queue is empty
bool Queue::isEmpty()
{
	return frontNode == NULL;
};

bool Queue::isIDExist(int id)
{
	Queue tempQueue;
	Booking b;
	bool found = false;
	if (!isEmpty())
	{
		while (!isEmpty())
		{
			getFront(b);
			if (b.getid() == id)
			{
				found = true;
			}
			tempQueue.enqueue(b);
			dequeue();
		}
		while (!tempQueue.isEmpty())
		{
			tempQueue.getFront(b);
			enqueue(b);
			tempQueue.dequeue();
		}
	}
	return found;
}

// display objs in queue from front to back
void Queue::displayItems()
{
	Booking item;
	Queue tempQueue;
	if (!isEmpty())
	{
		while (!isEmpty())
		{
			getFront(item);
			cout << item.getGuestName() << endl;
			tempQueue.enqueue(item);
			dequeue();
		}
		while (!tempQueue.isEmpty())
		{
			tempQueue.getFront(item);
			enqueue(item);
			tempQueue.dequeue();
		}
	}
	else
	{
		cout << "There is no records.\n";
	}
}

Queue Queue::displayByDate(string date)
{
	Booking item;
	Queue tempQueue;
	if (!isEmpty())
	{
		tm date1;
		char* d1String;
		string s1 = date;
		d1String = &s1[0];
		sscanf_s(d1String, "%d/%d/%4d",
			&date1.tm_mday, &date1.tm_mon, &date1.tm_year);

		tm targetDate = date1;
		int tDate = targetDate.tm_year * 10000 + targetDate.tm_mon * 100 + targetDate.tm_mday;

		while (!isEmpty())
		{
			getFront(item);
			
			tm date2;
			char* d2String;
			string s2 = item.getCheckInDate();
			d2String = &s2[0];
			sscanf_s(d2String, "%d/%d/%4d",
				&date2.tm_mday, &date2.tm_mon, &date2.tm_year);

			tm CheckInDate = date2;
			int ciDate = CheckInDate.tm_year * 10000 + CheckInDate.tm_mon * 100 + CheckInDate.tm_mday;

			tm date3;
			char* d3String;
			string s3 = item.getCheckOutDate();
			d3String = &s3[0];
			sscanf_s(d3String, "%d/%d/%4d",
				&date3.tm_mday, &date3.tm_mon, &date3.tm_year);

			tm CheckOutDate = date3;
			int coDate = CheckOutDate.tm_year * 10000 + CheckOutDate.tm_mon * 100 + CheckOutDate.tm_mday;

			if (tDate >= ciDate && tDate <= coDate)
			{
				cout << item.getGuestName() << endl;
			}
			tempQueue.enqueue(item);
			dequeue();
		}
		return tempQueue;
	}
	else
	{
		cout << "There is no records.\n";
		return tempQueue;
	}
}

Queue Queue::displayByMonth(string s)
{
	ItemType item;
	Queue tempQueue;
	if (!isEmpty())
	{
		while (!isEmpty())
		{
			getFront(item);

			if (strstr(item.getCheckInDate().c_str(), s.c_str()) || strstr(item.getCheckOutDate().c_str(), s.c_str()))
			{
				cout << item.getRoomNo() << setw(21) << item.getCheckInDate() << setw(20) << item.getCheckOutDate() << endl;
			}
			tempQueue.enqueue(item);
			dequeue();
		}
		
		return tempQueue;
	}
	else
	{
		cout << "There is no records.\n";
		return tempQueue;
	}
}

Queue Queue::CheckDateOfBookings(ItemType item, int& value)
{
	Queue tempQueue;
	Booking first;
	if (!isEmpty())
	{
		tm stayInDate;
		char* siString;
		string s1 = item.getCheckInDate();
		siString = &s1[0];
		sscanf_s(siString, "%d/%d/%4d",
			&stayInDate.tm_mday, &stayInDate.tm_mon, &stayInDate.tm_year);

		tm siDate = stayInDate;
		int sinDate = siDate.tm_year * 10000 + siDate.tm_mon * 100 + siDate.tm_mday;

		tm stayOutDate;
		char* soString;
		string s2 = item.getCheckOutDate();
		soString = &s2[0];
		sscanf_s(soString, "%d/%d/%4d",
			&stayOutDate.tm_mday, &stayOutDate.tm_mon, &stayOutDate.tm_year);

		tm soDate = stayOutDate;
		int soutDate = soDate.tm_year * 10000 + soDate.tm_mon * 100 + soDate.tm_mday;

		getFront(first);
		while (!isEmpty())
		{
			Booking next;
			tempQueue.enqueue(first);
			dequeue();
			getFront(next);
			
			tm CheckOutDate;
			char* ciString;
			string s3 = first.getCheckOutDate();
			ciString = &s3[0];
			sscanf_s(ciString, "%d/%d/%4d",
				&CheckOutDate.tm_mday, &CheckOutDate.tm_mon, &CheckOutDate.tm_year);

			tm coDate = CheckOutDate;
			int coutDate = coDate.tm_year * 10000 + coDate.tm_mon * 100 + coDate.tm_mday;
			tm checkInDate;
			char* coString;
			string s4 = next.getCheckInDate();
			coString = &s4[0];
			sscanf_s(coString, "%d/%d/%4d",
				&checkInDate.tm_mday, &checkInDate.tm_mon, &checkInDate.tm_year);

			tm ciDate = checkInDate;
			int cinDate = ciDate.tm_year * 10000 + ciDate.tm_mon * 100 + ciDate.tm_mday;
			if (sinDate >= coutDate && soutDate <= cinDate)
			{
				item.setStatus("Checked In");
				item.setRoomNo(first.getRoomNo());
				value = 1;
				tempQueue.enqueue(item);
				while (!isEmpty())
				{
					getFront(first);
					tempQueue.enqueue(first);
					dequeue();
				}
					
			}
			if (next.getCheckOutDate() == "")
			{
				if (sinDate >= coutDate)
				{
					if (first.getStatus() == "Checked Out")
					{
						item.setStatus("Checked In");
						item.setRoomNo(first.getRoomNo());
						value = 1;
						tempQueue.enqueue(item);
					}
				}
			}
			first = next;
		}
		return tempQueue;
	}
	else
	{
		return tempQueue;
	}
}