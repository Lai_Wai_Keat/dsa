// List.cpp - Implementation of List ADT using Array

#include "List.h"  // header file for List ADT

// constructor
List::List() { size = 0; }

// add an item to the back of the list (append)
bool List::add(string item)
{
    bool success = size < LIST_MAX_SIZE;
    if (success)
    {
        items[size] = item;  // add to the end of the list
        size++;              // increase the size by 1
    }
    return success;
}

// add an item at a specified position in the list (insert)
bool List::add(int index, string item)
{
    bool success = (index >= 0) && (index <= size) && (size < LIST_MAX_SIZE);
    if (success)
    {    // make room for the item by shifting all items at
         // positions > index toward the end of the
         // List (no shift if index == size)
        for (int pos = size; pos > index; pos--)
            items[pos + 1] = items[pos];
        // insert new item
        items[index] = item;
        size++;  // increase the size of the list by one
    }
    return success;
}
// remove an item at a specified position in the list
void List::remove(int index)
{
    bool success = (index >= 0) && (index < size);
    if (success)
    {   // delete item by shifting all items at positions >
        // index toward the beginning of the list
        // (no shift if index == size)
        for (int pos = index; pos < size; pos++)
            items[pos] = items[pos + 1];
        size--;  // decrease the size by 1
    }
}
// get an item at a specified position of the list (retrieve)
string List::get(int index)
{
    bool success = (index >= 0) && (index < size);
    if (success)
        return items[index];
    else
        return "-1";   // invalid index
}

// check the size of the list
int List::getLength() { return size; }
